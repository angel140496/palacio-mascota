<!doctype html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{$title}}</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Styles -->
        <style type="text/css">
            #inner {
                display: table;
                margin: 0 auto;
                width: 80%;
                text-align: center;
                padding: 25px 10px 25px 10px;
                border: 1px solid #a3e8ff;
            }

            #outer {
              width:100%
            }
        </style>
     </head>
    <body>
        <div id="outer">
            <div id="inner">
                <img style="width: 40%; height:auto;" src="https://palaciodelamascota.com/wp-content/uploads/2017/10/logo2.png" alt="logo">
                <h3>{{$title}}</h3>
                <h4>{{$text}}</h4>
            </div>
        </div>
    </body>
</html>

