<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
       	<meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
		
		<style>
			table {
				font-family: arial, sans-serif;
				border-collapse: collapse;
				width: 100%;
			}
			td, th {
				border: 1px solid #dddddd;
				text-align: left;
				padding: 5px;
			}
			th {
				background-color: #a3e8ff;
			}

			tr:nth-child(even) {
				background-color: #dddddd;
			}
		</style>

		
	</head>

	<body>
		<table>
			<thead>
				<tr>
					<td colspan="2">
						<img style="width: 200px; height: auto;" src="https://palaciodelamascota.com/wp-content/uploads/2017/10/logo2.png" alt="Zion">
					</td>
					<td colspan="4" style="text-align: center">
						<h4>DISTRIBUIDOR - IMPORTADOR - EXPORTADOR</h4>
					</td>
				</tr>
				<tr>
					<td colspan="6" style="text-align: center">
						<p>López de Gomara 652, Gllén., MDZ - Tel. 0261 421-7612 - Email: info@palaciodelamascota.com - WhatsApp +54 261 5998674</p>
					</td>
				</tr>
				<tr>
					<td colspan="4" width="20%" rowspan="" headers=""><b>Cliente: </b>{{$data['order']['cliente']}}</td>
					<td colspan="2" width="80%" rowspan="" headers=""><b>Tipo: </b>{{$data['order']['tipo']}}</td>
				</tr>
				<tr>
					<th width="5%">Art</th>
					<th width="15%">Código</th>
					<th width="20%">Cantidad</th>
					<th width="20%">Producto</th>
					<th width="20%">Unitario</th>
					<th width="20%">Total</th>
				</tr>
			</thead>
			<tbody>
				@php
					$i = 1
				@endphp
				@foreach ($data['productos'] as $sale)
					@php
						$i += 1
					@endphp
					<tr>
						<td>{{$i}}</td>
						<td>{{$sale['codigo']}}</td>
						<td>{{$sale['cantidad']}}</td>
						<td>{{$sale['producto']}}</td>
						<td>{{$sale['unico']}}</td>
						<td>{{$sale['total']}}</td>
					</tr>
				@endforeach
				<tr>
					<td style="background-color: #a3e8ff;" colspan="5" rowspan="" headers=""></td>
					<td colspan="" rowspan="" headers=""><b>TOTAL</b></td>
				</tr>
				<tr>
					<td style="background-color: #a3e8ff;" colspan="5" rowspan="" headers=""></td>
					<td colspan="" rowspan="" headers="">{{$data['order']['total']}}</td>
				</tr>
			</tbody>

		</table>
	</body>
</html>	