<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Auth
Route::post('/login', 					['as' => 'login', 			'uses' => 'LoginController@login']);
Route::get('/me', 						['as' => 'me', 				'uses' => 'UserController@me']);
Route::get('/logout',					['as' => 'logout',			'uses' => 'UserController@logout']);

//Users
Route::post('/users/create',		  ['as' => 'user.create',		'uses' => 'UserController@create']);
Route::get('/users',		 		  ['as' => 'user.index',		'uses' => 'UserController@index']);

//Products
Route::post('/products/create',		  ['as' => 'product.create',		'uses' => 'ProductController@create']);
Route::get('/products',		  	  	  ['as' => 'product.index',			'uses' => 'ProductController@index']);
Route::post('/products/upload',		  ['as' => 'product.upload',		'uses' => 'ProductController@upload']);

//Pre-Order
Route::post('/preorder/add',		  ['as' => 'product.preOrderAdd',	'uses' => 'ProductController@preOrderAdd']);
Route::get('/preorder',		     	  ['as' => 'product.preOrder',		'uses' => 'ProductController@preOrder']);
Route::get('/preorder/delete',     	  ['as' => 'product.deleteProductOrder',		'uses' => 'ProductController@deleteProductOrder']);
Route::get('/preorder/cancel',     	  ['as' => 'product.cancel',		'uses' => 'ProductController@cancel']);
Route::post('/preorder/order',     	  ['as' => 'product.toOrder',		'uses' => 'ProductController@toOrder']);
