<?php
namespace App\Transformers;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Storage;

/**
 *
 */
class ProductTransformer extends TransformerAbstract
{
  protected $defaultIncludes;
  public function transform($producto)
  {
    $this->defaultIncludes = [];
    $response = [
      'id'                => (int)$producto->id,
      'name'              => $producto->name,
      'mayoreoPrice'      => $producto->mayorista,
      'menudeoPrice'      => $producto->minorista,
      'code'              => $producto->email,
      'image'             => (Storage::disk('public')->exists($producto->email.'.jpg') ? env('APP_URL', 'http://177.231.202.103/palacio-mascota/public').'/storage/'.$producto->email.'.jpg?date='.date("Y-m-dh:i:s") : env('APP_URL', 'http://177.231.202.103/palacio-mascota/public').'/storage/'.'default.jpg')
    ];
    return $response;
  }
}