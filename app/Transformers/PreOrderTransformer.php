<?php
namespace App\Transformers;
use League\Fractal\TransformerAbstract;
use Illuminate\Support\Facades\Storage;

/**
 *
 */
class PreOrderTransformer extends TransformerAbstract
{
  protected $defaultIncludes;
  public function transform($producto)
  {
    $this->defaultIncludes = [];

    $response = [
      'id'                => (int)$producto->product->id,
      'name'              => $producto->product->name,
      'mayoreoPrice'      => $producto->product->mayorista,
      'menudeoPrice'      => $producto->product->minorista,
      'code'              => $producto->product->email,
      'client'            => ($producto->preorder ? $producto->preorder->cliente : null),
      'image'             => (Storage::disk('public')->exists($producto->product->email.'.jpg') ? env('APP_URL', 'http://177.231.202.103/palacio-mascota/public').'/storage/'.$producto->product->email.'.jpg?date='.date("Y-m-dh:i:s") : env('APP_URL', 'http://177.231.202.103/palacio-mascota/public').'/storage/'.'default.jpg?date='.date("Y-m-dh:i:s")),
      'quantity'          => $producto->quantity
    ];
    return $response;
  }
}