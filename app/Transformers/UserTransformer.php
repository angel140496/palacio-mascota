<?php
namespace App\Transformers;
use League\Fractal\TransformerAbstract;
/**
 *
 */
class UserTransformer extends TransformerAbstract
{
  protected $defaultIncludes;
  public function transform($usuario)
  {
    $this->defaultIncludes = [];
    $response = [
      'id'                => (int)$usuario->id,
      'nombre'            => $usuario->name,
      'tipo'              => $usuario->type,
      'clave'             => $usuario->email
    ];
    return $response;
  }
}