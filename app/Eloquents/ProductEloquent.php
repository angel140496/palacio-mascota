<?php
namespace App\Eloquent;
use App\Product;
use App\OrderProduct;
use App\PreOrder;
use App\Order;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Spipu\Html2Pdf\Html2Pdf;
use App\Providers\Email;


/**
 *  Class for Products interaction with Product Model
 */
class ProductEloquent
{
	public function create($request)
	{
		DB::table('products')->delete();
		foreach ($request->productos as $key => $Product) {
			$data = Product::where('email',$Product['clave'])->first();
			if ($data) {
				$query 				= Product::find($data->id);
				$query->name 		= $Product['nombre'];
				$query->mayorista 	= $Product['mayorista'];
				$query->minorista 	= $Product['minorista'];
				$query->save();
			} else {
				$query 				= new Product;
				$query->name 		= $Product['nombre'];
				$query->email 		= $Product['clave'];
				$query->mayorista 	= $Product['mayorista'];
				$query->minorista 	= $Product['minorista'];
				$query->save();
			}
		}

		return $query;
	}

	public function all()
	{
		return Product::all();
	}

	public function makeArray($data)
	{
		$array 	= [];
		unset($data[0]);
		foreach ($data as $key => $line) {
			if (count($line)==4) {
				$line[2] = floatval(str_replace(',','.',$this->formatter($line[2])));
				$line[3] = floatval(str_replace(',','.',$this->formatter($line[3])));
				$array[] = [
					'nombre'		=> utf8_encode($line[1]),
					'clave' 		=> utf8_encode($line[0]),
					'mayorista' 	=> number_format($line[2], 2, '.', ''),
					'minorista' 	=> number_format($line[3], 2, '.', '')
				];

			}
		}
		return $array;	
	}

	/**
	 * Uploead and deletes preview picture with the 
	 */
    public function picture($request)
    {
		Storage::disk('public')->delete($request->nombre.'.'.$request->extención);
        $photo = $this->saveImage($request->foto, $request->nombre);
        return $photo;
    }

	public function validate($data)
	{
		$pass = false;
		$header = ['codigo','producto', 'mayorista', 'minorista'];
		$data = $data[0];

		if ($this->formatter($data[0]) == $header[0] && $this->formatter($data[1]) == $header[1] && $this->formatter($data[2]) == $header[2] && $this->formatter($data[3]) == $header[3]) {
			$pass = true;
		}
		return $pass;	
	}

	public function formatter($string)
	{
		$string = str_replace(array('.', ' ', "\n", "\t", "\r"), '', $string);
		return utf8_encode($string);
	}

    private function saveImage($file, $name)
    {
        $mimes = [
          'png'  => 'data:image/png',
          'jpg'  => 'data:image/jpeg',
          'jpeg' => 'data:image/jpeg'
        ];
        $date = date("YmdHis");
        $extension = '';
        $file_mime = explode(';', $file);
        foreach ($mimes as $ext => $mime) {
          if ($ext === $file_mime[0]) {
            $extension = $ext;
            $file = str_replace('base64,', '', $file_mime[2]);
          }
        }
        $name .= ".$extension";
        $log = Storage::disk('public')->put($name, base64_decode($file));
        return ($log!=false ? $name : $log);
    }

    public function preOrder($request, $id)
    {
    	$producto = new OrderProduct;
    	$producto->preorder_id = $id;
    	$producto->quantity	   = $request->cantidad;
    	$producto->product_id  = $request->códigoDeProducto;
    	$producto->subtotal    = 0;
    	$producto->save();

    	if ($request->cliente) {
    		$preorder = PreOrder::find($id);
    		$preorder->cliente = $request->cliente;
    		$preorder->save();
    	}
    }

    public function toOrder($request, $id)
    {
    	$data = ['order' => [], 'productos'=>[]];
    	$order = new Order;
    	$order->user_id 		= $id;	
    	$order->total 			= $request->total;
    	$order->client 			= $request->cliente;
    	$order->type 			= $request->tipo;
    	$order->save();
    	$data['order'] = [
    		'total'		=> 	number_format($order->total,2, ',', ''),
    		'cliente'	=>	$order->client,
    		'tipo'		=>	$order->type
    	];
    	if ($order) {
			foreach ($request->productos as $key => $product) {
		    	$producto = new OrderProduct;
		    	$producto->order_id    = $order->id;
		    	$producto->quantity	   = $product['cantidad'];
		    	$producto->product_id  = $product['códigoDeProducto'];
		    	$producto->subtotal    = $product['subtotal'];
		    	$producto->save();
		    	$data['productos'][] = [
		    		'cantidad'	=>	number_format($producto->quantity,2, ',', ''),
		    		'total'	    => 	number_format($producto->subtotal,2, ',', ''),
		    		'codigo'	=>  Product::find($producto->product_id)->email,
		    		'producto'	=>  Product::find($producto->product_id)->name,
		    		'unico'		=>  number_format($producto->subtotal/$producto->quantity, 2,',', ''),

		    	];
			}
    	}
    	$this->cancelpreorder($id);
    	$this->getOrderPdf($data);
    	return $order;
    }

    private function getOrderPdf($data)
    {
		$html2pdf = new Html2Pdf();
		$html2pdf->writeHTML(view('order', ['data' => $data]));
		$file = env('SAVE').date("YmdHis").'.pdf';
		$html2pdf->output($file, 'F');
		$mail = new Email;
		$text = 'Nuevo pedido realizado. A continuación se adjuntó el archivo PDF correspondiente al pedido realizado.';
		$title= 'Palacio de la mascota';
		$transmitter=env('MAIL_USERNAME');
		$subject= 'Nuevo pedido realizado';
		$mail->sendEmailAttach($transmitter, [env('MAIL_TO')], [$file], 'text/plain', $title, $text, $subject);
    }

    public function getPreOrder($id)
    {
    	$preOrder = OrderProduct::where('preorder_id', $id)->orderby('created_at','asc')->with(['product', 'preorder']);
    	$preOrder = $preOrder->get();
    	return $preOrder;
    }

    public function deletepreorder($id)
    {
    	$preOrder = OrderProduct::where('product_id', $id)->delete();
    	return $preOrder;
    }
    public function cancelpreorder($id)
    {
    	$preorder = OrderProduct::where('preorder_id', $id)->delete();
		$preorder = PreOrder::find($id);
		$preorder->cliente = null;
		$preorder->save();
    	return $preorder;
    }
}