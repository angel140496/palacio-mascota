<?php
namespace App\Eloquent;
use App\User;
use App\PreOrder;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

/**
 *  Class for users interaction with User Model
 */
class UserEloquent
{
	public function create($request)
	{
		DB::table('users')->delete();
		foreach ($request->data as $key => $user) {
			$data = User::where('email',$user['clave'])->first();
			if ($data) {
				$query 			= User::find($data->id);
				$query->name 	= $user['nombre'];
				$query->type 	= $user['tipo'];
				$query->save();
			} else {
				$query 			= new User;
				$query->name 	= $user['nombre'];
				$query->email 	= $user['clave'];
				$query->password= Hash::make('mascota');
				$query->type 	= $user['tipo'];
				$query->save();

				$data = new PreOrder;
				$data->user_id 	= $query->id;
				$data->total 	= 0;
				$data->save();
			}
		}

		return $query;
	}

	public function all()
	{
		return User::all();
	}

	public function validate($data)
	{
		$pass = false;
		$header = ['nombre', 'clave', 'tipo'];
		$data = $data[0];

		if ($this->formatter($data[0]) == $header[0] && $this->formatter($data[1]) == $header[1] && $this->formatter($data[2]) == $header[2]) {
			$pass = true;
		}
		return $pass;	
	}

	public function formatter($string)
	{
		$string = str_replace(array('.', ' ', "\n", "\t", "\r"), '', $string);
		return utf8_encode($string);
	}

	public function makeArray($data)
	{
		$array 	= [];
		unset($data[0]);
		foreach ($data as $key => $line) {
			if (count($line)==3) {
				$array[] = [
					'nombre'=> utf8_encode($line[0]),
					'clave' => $this->formatter($line[1]),
					'tipo' 	=> $this->formatter($line[2]),
				];
			}

		}
		return $array;	
	}

}