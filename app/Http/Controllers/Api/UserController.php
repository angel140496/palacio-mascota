<?php

namespace App\Http\Controllers;

use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Eloquent\UserEloquent;
use Illuminate\Support\Facades\Auth;
use App\Transformers\UserTransformer;

class UserController extends Controller
{
    protected $user;
    public function __construct(UserEloquent $user, Auth $loggedUser)
    {
        $this->middleware('jwt.auth');
        $this->user = $user;
        $this->loggedUser = $loggedUser;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function create(Request $request)
    {
        $this->validate($request, ['csv' => 'required']);
        try {
            $data = explode(";", $request->csv);
            if ($data[0] == 'csv') {
                $file = base64_decode($data[1]);
                $lines = explode(PHP_EOL, $file);
                $array = array_map(function($val) {
                    return explode(';', $val);
                }, $lines);
                $validation = $this->user->validate($array);
                if ($validation) {
                    $array = $this->user->makeArray($array);
                    $request->merge(['data' => $array]);
                    $this->validate($request, [
                        'data.*.nombre' => 'required|string',
                        'data.*.clave'  => 'required|numeric',
                        'data.*.tipo'   => 'required|in:admin,cliente'
                    ]);
                    $this->user->create($request);
                    return $this->respondOk('Lista de usuarios actualizada correctamente');
                } else {
                    return $this->respondWithError("El archivo CSV no cuenta con la estructura definida, intente ingresando el CSV correcto.",401);
                }

            } else {
                return $this->respondWithError("El archivo debe ser de tipo CSV, {$data[0]} ingresado",401);
            }
        } catch (Exception $e) {
            return $this->respondWithError('Error al subir archivos de usuarios, íntentelo más tarde', 500);
        }
    }

    /**
    * Log the user out (Invalidate the token).
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function logout()
    {
        try {
            if (!$me = $this->loggedUser::user()) {
                return $this->respondWithError('Usuario no autorizado',401);
            } else {
                auth('api')->logout();
                return $this->respondOk('Sesión cerrada correctamente');
            }
        } catch (\Exception $e) {
            return $this->respondWithError('Usuario desconocido',401);
        }
    }

    /**
    * Get the authenticated User.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function me(Manager $fractal, UserTransformer $transform)
    {
        try {
            if (!$me = $this->loggedUser::user()) {
                return $this->respondWithError('Usuario no autorizado',401);
            } else {
                $item = new Item($this->loggedUser::user(), $transform);
                $data = $fractal->createData($item)->toArray();
                return $this->respondOkCORS($data);
            }
        } catch (\Exception $e) {
            return $this->respondWithError('Usuario desconocido',401);
        }
    }

    /**
    * Get the authenticated User.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function index(Manager $fractal, UserTransformer $transform)
    {
        try{
            $data = $this->user->all();
            $item = new Collection($data, $transform);
            $data = $fractal->createData($item)->toArray();
            return $this->respondOkCORS($data);
        } catch (\Exception $e) {
            return $this->respondWithError('Usuario desconocido',401);
        }
    }

}
