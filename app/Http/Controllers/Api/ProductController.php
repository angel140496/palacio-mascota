<?php

namespace App\Http\Controllers;

use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Manager;
use Illuminate\Http\Request;
use App\Eloquent\ProductEloquent;
use Illuminate\Support\Facades\Auth;
use App\Transformers\ProductTransformer;
use App\Transformers\PreOrderTransformer;

class ProductController extends Controller
{
    protected $product;
    public function __construct(ProductEloquent $product, Auth $loggedUser)
    {
        $this->middleware('jwt.auth');
        $this->product = $product;
        $this->loggedUser = $loggedUser;
    }

    /**
     * Create a new product instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function create(Request $request)
    {
        $this->validate($request, ['csv' => 'required']);
        try {
            $data = explode(";", $request->csv);
            if ($data[0] == 'csv') {
                $file = base64_decode($data[1]);
                $lines = explode(PHP_EOL, $file);
                $array = array_map(function($val) {
                    return explode(';', $val);
                }, $lines);
                $validation = $this->product->validate($array);
                if ($validation) {
                    $array = $this->product->makeArray($array);
                    $request->merge(['productos' => $array]);
                    $this->validate($request, [
                        'productos'              => 'required',
                        'productos.*.nombre'     => 'required|string',
                        'productos.*.clave'      => 'required|string',
                        'productos.*.mayorista'  => 'required|numeric',
                        'productos.*.minorista'  => 'required|numeric',
                    ]);
                    $this->product->create($request);
                    return $this->respondOk('Lista de productos actualizada correctamente');
                } else {
                    return $this->respondWithError("El archivo CSV no cuenta con la estructura definida, intente ingresando el CSV correcto.",401);
                }

            } else {
                return $this->respondWithError("El archivo debe ser de tipo CSV, {$data[0]} ingresado",401);
            }
        } catch (Exception $e) {
            return $this->respondWithError('Error al subir archivo de productos, íntentelo más tarde', 500);
        }
    }


    /**
     * Uploads new picture.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function upload(Request $request)
    {
        $this->validate($request, [
            'foto'      => 'required',
            'extensión' => 'required',
            'nombre'    => 'required'
        ]);
        try {
            $response = $this->product->picture($request);
            if ($response) {
                return $this->respondOk('Imagen de producto subida correctamente');
            } else {
                return $this->respondWithError('Error al subir imagen de producto, íntentelo subir un archivo de tipo JPG, JPEG o PNG', 500);
            }
        } catch (Exception $e) {
            return $this->respondWithError('Error al subir imagen de producto, íntentelo más tarde', 500);
        }
    }

    /**
     * Add new pedido.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function preOrderAdd(Request $request)
    {
        $this->validate($request, [
            'códigoDeProducto'      => 'required|numeric|exists:products,id',
            'cantidad'              => 'required|numeric',
            'cliente'               => ''
        ]);
        try {
            $response = $this->product->preOrder($request, $this->loggedUser::user()->id);
            return $this->respondOk('Producto agregado al pre-pedido');
        } catch (Exception $e) {
            return $this->respondWithError('Error al subir producto, íntentelo más tarde', 500);
        }
    }

    /**
     * Add new pedido.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function preOrder(Manager $fractal,PreOrderTransformer $transform)
    {
        try {
            $data = $this->product->getPreOrder($this->loggedUser::user()->id);
            $item = new Collection($data, $transform);
            $data = $fractal->createData($item)->toArray();
            return $this->respondOkCORS($data);
        } catch (Exception $e) {
            return $this->respondWithError('Error al obtener pre pedido, íntentelo más tarde', 500);
        }
    }

    /**
    * Get Products.
    *
    * @return \Illuminate\Http\JsonResponse
    */
    public function index(Manager $fractal, ProductTransformer $transform)
    {
        try{
            $data = $this->product->all();
            $item = new Collection($data, $transform);
            $data = $fractal->createData($item)->toArray();
            return $this->respondOkCORS($data);
        } catch (\Exception $e) {
            return $this->respondWithError('Error al obtener lista de productos',500);
        }
    }

    /**
     * delete Products pre order.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProductOrder(Request $request)
    {
        $this->validate($request, [
            'códigoDeProducto'      => 'required|numeric|exists:pedidos_productos,product_id',
        ]);
        try{
            $data = $this->product->deletepreorder($request->códigoDeProducto);
            return $this->respondOk('Producto eliminado correctamente');
        } catch (\Exception $e) {
            return $this->respondWithError('Error al obtener lista de productos',500);
        }
    }

    /**
     * delete Products pre order.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancel(Request $request)
    {

        try {
            $data = $this->product->cancelpreorder($this->loggedUser::user()->id);
            return $this->respondOk('Pedido cancelado correctamente');
        } catch (\Exception $e) {
            return $this->respondWithError('Error al obtener lista de productos',500);
        }
    }

    /**
     * delete Products pre order.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function toOrder(Request $request)
    {
        $this->validate($request, [
            'total'                             => 'required',
            'tipo'                              => 'required|in:mayoreo,menudeo',
            'cliente'                           => 'required|string',
            'productos'                         => 'required|array',
            'productos.*.códigoDeProducto'      => 'required|numeric|exists:products,id',
            'productos.*.cantidad'              => 'required|numeric',
            'productos.*.subtotal'              => 'required'
        ]);
        try {
            $data = $this->product->toOrder($request, $this->loggedUser::user()->id);
            return $this->respondOk('Pedido realizado correctamente');
        } catch (\Exception $e) {
            return $this->respondWithError('Error al obtener lista de productos',500);
        }
    }





}
