<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
	protected function respondWithToken($token)
	{
		return response()->json([
		    'token' => $token,
		    'token_type' => 'bearer',
		    'expires_in' => Auth::factory()->getTTL() * 60,
		    'validation' => Auth::user()->validation
		], 200);
	}

	/**
	* @param  array|string $message
	* @param  integer      $statusCode
	* @param  array        $headers
	* @return \Illuminate\Http\JsonResponse
	*/
	protected function respond($message, $statusCode = 200, $headers = [])
	{
		return response()->json($message, $statusCode, $headers);
	}

	/**
	* @param  array|string $message
	* @param  integer      $statusCode
	* @param  array        $headers
	* @return \Illuminate\Http\JsonResponse
	*/
	protected function respondOk($message, $statusCode = 200, $headers = [])
	{
		return $this->respond([
		  'errors'      => false,
		  'data'        => $message,
		  'status_code' => $statusCode,
		], $statusCode, $headers);
	}

	/**
	* @param  array|string $message
	* @param  integer      $statusCode
	* @param  array        $headers
	* @param  array        $dataExt
	* @return \Illuminate\Http\JsonResponse
	*/
	protected function respondWithError($message, $statusCode = 400, $headers = [], $dataExt = null)
	{
		$response = [
		  'errors'      => $message,
		  'status_code' => $statusCode,
		];
		if ($dataExt !== null) {
		  $response['data'] = $dataExt;
		}
		return $this->respond($response, $statusCode, $headers);
	}

	protected function respondOkCORS($data, $statusCode = 200)
	{
		return $this->respond(array_merge([
		  'errors'      => false,
		  'status_code' => $statusCode,
		], $data),
		  $statusCode, $this->setCORSHeaders());
	}

	private function setCORSHeaders()
	{
		$header['Access-Control-Allow-Origin']      = '*';
		$header['Allow']                            = 'GET, POST, OPTIONS';
		$header['Access-Control-Allow-Headers']     = 'Origin, Content-Type, Accept, Authorization';
		$header['Access-Control-Allow-Credentials'] = 'true';
		return $header;
	}
}
