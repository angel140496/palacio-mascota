<?php
namespace App\Providers;
use Illuminate\Support\Facades\Mail;
/**
 * Class to send a single email or a collection of emails
 */
class Email
{
  public function sendEmailItem($transmitter, $reciver, $reciver2, $title='Title', $text='Text example here', $subject='Notification')
  {
    // $transmitter = 'zionst.app@gmail.com';
    $data = array('title' => $title, 'text' => $text);
    Mail::send('email', $data, function ($message) use($transmitter, $reciver, $subject, $title) {
      $message->from($transmitter, $title);
      $message->to($reciver)->subject($subject);
    });
    if ($reciver2) {
      \Mail::send('email', $data, function ($message) use($transmitter, $reciver2, $subject, $title) {
        $message->from($transmitter, $title);
        $message->to($reciver2)->subject($subject);
      });
    }

    
  }

  public function sendEmailCollection($transmitter, $recivers=[], $title='Title', $text='Text example here', $subject='Notification')
  {
    // $transmitter = 'zionst.app@gmail.com';
    foreach ($recivers as $reciver) {
      $data = array('title' => $title, 'text' => $text);
      Mail::send('email', $data, function ($message) use($transmitter, $reciver, $subject, $title) {
        $message->from($transmitter, $title);
        $message->to($reciver)->subject($subject);
      });
    }
  }

  public function sendEmailAttach($transmitter, $recivers=[], $paths, $mime, $title='Title', $text='Text example here', $subject='Notification')
  {
    // $transmitter = 'zionst.app@gmail.com';
    foreach ($recivers as $reciver) {
      $data = array('title' => $title, 'text' => $text);
      Mail::send('email', $data, function ($message) use($transmitter, $reciver, $subject, $title, $paths, $mime) {
        $message->from($transmitter, $title);
        $message->to($reciver)->subject($subject);
        if (count($paths)>0) {
          foreach ($paths as $key => $path) {
            $message->attach($path);
          }
        }
      });
    }
  }

}

