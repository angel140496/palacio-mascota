<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'pedidos_productos';
    protected $fillable = [
        'order_id', 'preorder_id','product_id','quiantity','subtotal'
    ];

    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function preorder()
    {
        return $this->belongsTo('App\PreOrder', 'preorder_id');
    }
}