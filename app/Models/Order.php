<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'pedidos';
    protected $fillable = [
        'user_id', 'total'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct', 'order_id');
    }
}