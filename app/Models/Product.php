<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'products';
    protected $fillable = [
        'name', 'email', 'mayorista', 'minorista'
    ];

    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct', 'product_id');
    }
}