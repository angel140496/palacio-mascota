<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class PreOrder extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'pre_pedido';
    protected $fillable = [
        'user_id', 'total'
    ];
    protected $primaryKey = 'user_id';


    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function orderProducts()
    {
        return $this->hasMany('App\OrderProduct', 'preorder_id');
    }
}